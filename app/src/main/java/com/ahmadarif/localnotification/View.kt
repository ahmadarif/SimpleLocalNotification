package com.ahmadarif.localnotification

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import java.util.*

/**
 * Created by ARIF on 10-Jun-17.
 */
fun Activity.delayNotif(second: Int, title: String, contentText: String) {
    val notificationIntent = Intent("android.media.action.DISPLAY_NOTIFICATION")
    notificationIntent.addCategory("android.intent.category.DEFAULT")
    notificationIntent.putExtra("title", title)
    notificationIntent.putExtra("contentText", contentText)

    val broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

    val cal = Calendar.getInstance()
    cal.add(Calendar.SECOND, second)

    val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.timeInMillis, broadcast)
}