package com.ahmadarif.localnotification

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_answer_receive.*


class AnswerReceiveActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer_receive)

        Log.d("Main", intent.action)
        tvAnswerReceiveText.text = intent.action
    }
}
