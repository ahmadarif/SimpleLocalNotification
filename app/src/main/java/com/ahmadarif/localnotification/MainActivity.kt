package com.ahmadarif.localnotification

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    var currentNotificationID = 0

    lateinit var notificationManager: NotificationManager
    lateinit var notificationBuilder: NotificationCompat.Builder

    lateinit var notificationTitle: String
    lateinit var notificationText: String
    lateinit var icon: Bitmap
    var combinedNotificationCounter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        icon = BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher)
        bindWidgetWithAnEvent()

        Log.d(this.localClassName, "Token = " + FirebaseInstanceId.getInstance().token)
    }

    private fun bindWidgetWithAnEvent() {
        btnDelayNotification.setOnClickListener(this)
        btnMainSendSimpleNotification.setOnClickListener(this)
        btnMainSendExpandLayoutNotification.setOnClickListener(this)
        btnMainSendNotificationActionBtn.setOnClickListener(this)
        btnMainSendMaxPriorityNotification.setOnClickListener(this)
        btnMainSendMinPriorityNotification.setOnClickListener(this)
        btnMainSendCombinedNotification.setOnClickListener(this)
        btnMainClearAllNotification.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        setNotificationData()

        when(v) {
            btnDelayNotification -> {
                setNotificationData()
                delayNotif(10, notificationTitle, notificationText)
            }
            btnMainSendSimpleNotification -> setDataForSimpleNotification()
            btnMainSendExpandLayoutNotification -> setDataForExpandLayoutNotification()
            btnMainSendNotificationActionBtn -> setDataForNotificationWithActionButton()
            btnMainSendMaxPriorityNotification -> setDataForMaxPriorityNotification()
            btnMainSendMinPriorityNotification -> setDataForMinPriorityNotification()
            btnMainSendCombinedNotification -> setDataForCombinedNotification()
            btnMainClearAllNotification -> clearAllNotifications()
        }
    }

    private fun sendNotification() {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        notificationBuilder.setContentIntent(contentIntent)

        val notification = notificationBuilder.build()
        notification.flags = Notification.FLAG_AUTO_CANCEL
        notification.defaults = Notification.DEFAULT_SOUND
        currentNotificationID++

        var notificationId = currentNotificationID

        if (notificationId == Integer.MAX_VALUE - 1) notificationId = 0
        notificationManager.notify(notificationId, notification)
    }

    private fun clearAllNotifications() {
        currentNotificationID = 0
        notificationManager.cancelAll()
    }

    private fun setNotificationData() {
        notificationTitle = this.getString(R.string.app_name)
        notificationText = "Hello..This is a Notification Test"

        if (etMainNotificationText.text.toString() != "") {
            notificationText = etMainNotificationText.text.toString()
        }

        if (etMainNotificationTitle.text.toString() != "") {
            notificationTitle = etMainNotificationTitle.text.toString()
        }
    }

    private fun setDataForSimpleNotification() {
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
        sendNotification()
    }

    private fun setDataForExpandLayoutNotification() {
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationText))
                .setContentText(notificationText)
        sendNotification()
    }

    private fun setDataForNotificationWithActionButton() {
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationText))
                .setContentText(notificationText)

        val answerIntent = Intent(this, AnswerReceiveActivity::class.java)
        answerIntent.action = "Yes"

        val pendingIntentYes = PendingIntent.getActivity(this, 1, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        notificationBuilder.addAction(R.drawable.ic_thumb_up, "Yes", pendingIntentYes)

        answerIntent.action = "No"
        val pendingIntentNo = PendingIntent.getActivity(this, 1, answerIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        notificationBuilder.addAction(R.drawable.ic_thumb_down, "No", pendingIntentNo)

        sendNotification()
    }

    private fun setDataForMaxPriorityNotification() {
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationText))
                .setPriority(Notification.PRIORITY_MAX)
                .setContentText(notificationText)
        sendNotification()
    }

    private fun setDataForMinPriorityNotification() {
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationText))
                .setPriority(Notification.PRIORITY_MIN)
                .setContentText(notificationText)
        sendNotification()
    }

    private fun setDataForCombinedNotification() {
        combinedNotificationCounter++
        notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(icon)
                .setContentTitle(notificationTitle)
                .setGroup("group_emails")
                .setGroupSummary(true)
                .setContentText("$combinedNotificationCounter new messages")

        val inboxStyle = NotificationCompat.InboxStyle()
        inboxStyle.setBigContentTitle(notificationTitle)
        inboxStyle.setSummaryText("mehulrughani@gmail.com")

        for (i in (1..combinedNotificationCounter)) {
            inboxStyle.addLine("This is Test" + i)
        }

        currentNotificationID = 500
        notificationBuilder.setStyle(inboxStyle)
        sendNotification()
    }
}